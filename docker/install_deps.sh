#!/bin/sh

# Install packages required for CMake
yum install -y curl-devel make

# Install packages required for ParaView
yum install -y \
    git-core chrpath libtool gperf\
    libX11-devel libXdamage-devel libXext-devel libXt-devel libXi-devel \
    libxcb-devel xorg-x11-xtrans-devel libXcursor-devel libXft-devel \
    libXinerama-devel libXrandr-devel libXrender-devel \
    mesa-libGL-devel mesa-libGLU-devel mesa-dri-drivers \
    dejavu-sans-fonts dejavu-sans-mono-fonts dejavu-serif-fonts \
    xkeyboard-config which

# Install packages required for Qt
yum install -y \
    libxcb libxcb-devel xcb-util xcb-util-devel mesa-libGL-devel libxkbcommon-devel

# Determine CentOS version  
VERSION_ID=6
# This file exists on CentOS 7 and newer
if [ -e /etc/os-release ]; then . /etc/os-release; fi
VAULT_RELEASE=
case $VERSION_ID in
    6) VAULT_RELEASE=6.9;;
    7) VAULT_RELEASE=7.6.1810;;
esac

# Install a newer set of compilers from the Software Collections repos
if [ -z $VAULT_RELEASE ]; then
    yum install -y centos-release-scl yum-utils
    yum-config-manager --enable centos-sclo-rh-testing
else
    # Use packages from vault.centos.org
    yum install -y centos-release-scl-rh yum-utils
    yum-config-manager --enable centos-sclo-rh
    yum-config-manager --setopt=centos-sclo-rh.baseurl=http://vault.centos.org/centos/$VAULT_RELEASE/sclo/\$basearch/rh/ --save
    # Disable mirrors, as they typically do not carry the vault
    sed -i -e 's/^mirrorlist=/##mirrorlist=/g' /etc/yum.repos.d/CentOS-SCLo-scl-rh.repo
fi

yum install -y \
    devtoolset-6-gcc devtoolset-6-gcc-c++ devtoolset-6-gcc-gfortran \
    devtoolset-4-gcc devtoolset-4-gcc-c++ devtoolset-4-gcc-gfortran \
    python27

# Cleanup
yum clean all
